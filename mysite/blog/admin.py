from django.contrib import admin
from .models import Post, Comment

"""We are telling the Django admin site that our model is registered in
the admin site using a custom class that inherits from ModelAdmin. In
this class, we can include information about how to display the
model in the admin site and how to interact with it. The list_display
attribute allows you to set the fields of your model that you want to
display in the admin object list page. The @admin.register() decorator
performs the same function as the admin.site.register() function we
have replaced, registering the ModelAdmin class that it decorates."""
# admin.site.register(Post) replaced by the code below
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'publish', 'status',)
    list_filter = ('status', 'created', 'publish', 'author')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ('status', 'publish')

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'post', 'created', 'active')
    list_filter = ('active', 'created', 'updated')
    search_fields = ('name', 'email', 'body')
