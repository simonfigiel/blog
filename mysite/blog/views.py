from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from django.core.mail import send_mail
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
# Django ORM aggregation function which allows to use Avg, Max....
from django.db.models import Count

# 3rd party libraries
from taggit.models import Tag

# local stuff
from .models import Post
from .forms import EmailPostForm, CommentForm, SearchForm


# s.186 Django 2 by Example
def post_search(request):
    form = SearchForm()
    query = None
    results = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            # results = Post.objects.annotate(search=SearchVector('title', 'body'),).filter(search=query)
            search_vector = SearchVector('title', 'body')
            search_query = SearchQuery(query)
            results = Post.objects.annotate(
                search=search_vector,
                rank=SearchRank(search_vector, search_query)
            ).filter(search=search_query).order_by('-rank')
    return render(request, 'blog/post/search.html', {'form': form, 'query': query, 'results': results})


def post_share(request, post_id):
    # Retrieve post by id
    post = get_object_or_404(Post, id=post_id, status='published')
    sent = False

    if request.method == 'POST':
        # Form was submitted
        form = EmailPostForm(request.POST)
        if form.is_valid():
            # Form fields passed validation
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(
                post.get_absolute_url())
            subject = '{} ({}) recommends you reading "{}"'.format(cd['name'], cd['email'], post.title)
            message = 'Read "{}" at {}\n\n{}\'s comments: {}'.format(post.title, post_url, cd['name'], cd['comments'])
            send_mail(subject, message, 'admin@myblog.com',
                      [cd['to']])
            sent = True
    else:
        form = EmailPostForm()
    return render(request, 'blog/post/share.html', {'post': post,
                                                    'form': form,
                                                    'sent': sent})


def post_list(request, tag_slug=None):

    """
        1. We instantiate the Paginator class with the number of objects
            we want to display on each page.
        2. We get the page GET parameter that indicates the current page
            number.
        3. We obtain the objects for the desired page calling the page()
            method of Paginator.
        4. If the page parameter is not an integer, we retrieve the first
            page of results. If this parameter is a number higher than
            the last page of results, we will retrieve the last page.
        5. We pass the page number and retrieved objects to the
            template."""

    """
    TAGS
    1. It takes an optional tag_slug parameter that has a None default value.
     This parameter will come in the URL.
    
    2. Inside the view, we build the initial QuerySet, retrieving all
    published posts, and if there is a given tag slug, we get the Tag
    object with the given slug using the get_object_or_404() shortcut.
    
    3. Then, we filter the list of posts by the ones that contain the
    given tag. Since this is a many-to-many relationship, we
    have to filter by tags contained in a given list, which, in our
    case, contains only one element.
"""
    object_list = Post.published.all()
    paginator = Paginator(object_list, 3)  # 3 posts in each page
    page = request.GET.get('page')

    tag = None

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        for s in tag:
            print(s)
        # we filter the list of posts by the ones that contain
        # the given tag
        object_list = object_list.filter(tag__in=[tag])
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    return render(request,
                  'blog/post/list.html',
                  {'page': page,
                   'posts': posts,
                   'tag': tag})


class PostListView(ListView):
    print('PostListView get called')
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'


def post_detail(request, year, month, day, post):

    # this function retrieves the object that matches given parameters or launches an HTTP 404
    post = get_object_or_404(Post, slug=post,
                             status='published',
                             publish__year=year,
                             publish__month=month,
                             publish__day=day)
    # List of active comments for this post. related_name == 'comments'
    comments = post.comments.filter(active=True)

    """We also use the same view to let our users add a new comment.
    Therefore, we initialize the new_comment variable by setting it to None."""
    new_comment = None

    if request.method == 'POST':
        # A comment was posted
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment we just created
            new_comment.post = post
            """By doing this, we are specifying that the new comment
            belongs to this post.
            Save the comment to the database"""
            new_comment.save()
    else:
        comment_form = CommentForm()

    # List of similar posts
    """
    1. We retrieve a Python list of IDs for the tags of the current
        post. The values_list() QuerySet returns tuples with the
        values for the given fields. We pass flat=True to it to get a flat
        list like [1, 2, 3, ...].
    2. We get all posts that contain any of these tags, excluding the
        current post itself.
    3. We use the Count aggregation function to generate a
        calculated field—same_tags—that contains the number of tags shared with all the tags queried.
    4. We order the result by the number of shared tags
        (descending order) and by publish to display recent posts first
        for the posts with the same number of shared tags. We slice
        the result to retrieve only the first four posts.
    """
    post_tags_ids = post.tags.values_list('id', flat=True)
    similar_posts = Post.published.filter(tags__in=post_tags_ids) \
        .exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')) \
                        .order_by('-same_tags', '-publish')[:4]


    return render(request,
                  'blog/post/detail.html',
                  {'post': post,
                   'comments': comments,
                   'new_comment': new_comment,
                   'comment_form': comment_form,
                   'similar_posts': similar_posts})
