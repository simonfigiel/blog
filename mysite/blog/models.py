from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from taggit.managers import TaggableManager

class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager,
                     self).get_queryset()\
                        .filter(status='published')


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published')
    )
    title = models.CharField(max_length=250)
    """This is a field intended to be used in URLs. A slug is a
short label that contains only letters, numbers, underscores,
or hyphens. We will use the slug field to build beautiful, SEO friendly
URLs for our blog posts. We have added the
unique_for_date parameter to this field so that we can build
URLs for posts using their publish date and slug. Django will
prevent multiple posts from having the same slug for a
given date."""
    slug = models.SlugField(max_length=250,
                            unique_for_date='publish')
    """when user is deleted, the
  database will also delete its related blog posts"""
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               # This will allow us to access related objects easily.
                               related_name='blog_post')
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10,
                              choices=STATUS_CHOICES,
                              default='draft')
    tags = TaggableManager()
    # The default manager.
    objects = models.Manager()
    # Our custom manager.
    published = PublishedManager()

    class Meta:
        """sort results in the publish field descending by default
         when we query the database"""
        ordering = ('-publish',)

    def __str__(self):
        return self.title

    # return canonical URL for Post objects
    # we will use the reverse() method that allows you to build
    # URLs by their name and passing optional parameters.
    def get_absolute_url(self):
        return reverse('blog:post_detail',
                       args=[self.publish.year,
                             self.publish.month,
                             self.publish.day,
                             self.slug],)


# related_name s.131
"""
After defining
this, we can retrieve the post of a comment object using comment.post
and retrieve all comments of a post using post.comments.all(). If
"""

class Comment(models.Model):
    # associate the comment with a single post.
    """related_name attribute allows us to name the attribute that we use for
the relation from the related object back to this one. After defining
this, we can retrieve the post of a comment object using comment.post
and retrieve all comments of a post using post.comments.all().If you
don't define the related_name attribute, Django will use the name of the
model in lowercase, followed by _set (that is, comment_set) to name the
manager of the related object back to this one."""
    post = models.ForeignKey(Post,
                             on_delete=models.CASCADE,
                             related_name='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    # to deactivate inappropriate comments
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return 'test'
        # return f'Comment by {self.name} {self.post}'