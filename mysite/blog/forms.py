from django import forms
from .models import Comment


class SearchForm(forms.Form):
    query = forms.CharField()


class EmailPostForm(forms.Form):
    # <input type="text">
    name = forms.CharField(max_length=25)
    email = forms.EmailField()
    to = forms.EmailField()
    '''The default widget can be overridden with the widget attribute.We
    use a Textarea widget to display it as a <textarea> HTML element
    instead of the default <input> element.'''
    comments = forms.CharField(required=False, widget=forms.Textarea)


class CommentForm(forms.ModelForm):
    class Meta:
        """to indicate which model we wanna use. By default, Django builds a form field for each field
        contained in the model. However, you can explicitly tell the
        framework which fields you want to include in your form using a
        fields list or define which fields you want to exclude using an exclude
        list of fields."""
        model = Comment
        fields = ('name', 'email', 'body')
